using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigator : MonoBehaviour
{

    [SerializeField] GameObject character;
    [SerializeField] GameObject[] movePoint;

    void Start()
    {
        
    }

    private void Update()
    {
        Navigate();
    }

    IEnumerator Move()
    {
        int i;
        float totalMovementTime = 5.0f;
        float currentMovementTime = 0.0f;

        for (i=0; i < movePoint.Length; i++)
        {
            currentMovementTime += Time.deltaTime;

            character.transform.position = Vector3.Lerp(character.transform.position, movePoint[i].transform.position, totalMovementTime/currentMovementTime);

            yield return new WaitForSeconds(6.0f);
        }
    }

    

    void Navigate()
    {
        StartCoroutine(Move());
    }

}
