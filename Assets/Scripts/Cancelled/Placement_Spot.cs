using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placement_Spot : MonoBehaviour
{

    [SerializeField] Transform BlueTransform;

    [SerializeField] Transform TargetTransform;

    [SerializeField] GameObject BlueCollider;

    [SerializeField] GameObject TargetCollider;


    private bool BlueClick = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Blue"))
        {
            print("Blue Clicked.");
            BlueClick = true;
            print(BlueClick);
        }

        if(collision.CompareTag("Target"))
        {
            print("Target Clicked.");

            if (BlueClick == true)
            {
                BlueTransform = TargetTransform;
            }
        }
    }*/

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Blue"))
        {
            print("Blue Clicked.");
            BlueClick = true;
            print(BlueClick);
        }

        if (collision.gameObject.CompareTag("Target"))
        {
            print("Target Clicked.");

            if (BlueClick == true)
            {
                BlueTransform = TargetTransform;
            }
        }

    }
}
