using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DragDrop : MonoBehaviour
{
    public bool isDragging;
    private GameObject GameSystem;
    public GameObject SystemScript => GameSystem;
    public bool Paid;
    public AudioSource PickSFX;
    public AudioSource DropSFX;
    public AudioSource PaySFX;

    private void Start()
    {
        GameSystem = GameObject.Find("System");
        Paid = false;
    }

    public void OnMouseDown()
    {
        isDragging = true;
        PickSFX.Play();
    }

    public abstract void OnMouseUp();

    void Update()
    {
        GameObject PhaseScript = GameObject.Find("System");
        GamePhase GamePhaseScript = PhaseScript.GetComponent<GamePhase>();

        if (isDragging && GamePhaseScript.Phase == GameState.Prep)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePosition);
        }
    }
}
