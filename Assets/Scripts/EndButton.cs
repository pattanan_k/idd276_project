using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndButton : MonoBehaviour
{
    public string SceneName;

    public void ChangeToScene()
    {
        PlayerDataManager.instance.ClearPlayerData();
        SceneManager.LoadScene(SceneName);
    }
}
