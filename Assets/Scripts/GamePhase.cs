using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public enum GameState {Prep, Play}

public class GamePhase : MonoBehaviour
{
    public GameState Phase;
    [SerializeField] Player PlayerScript;
    [SerializeField] Transform Goal_1;
    [SerializeField] Transform Goal_2;
    [SerializeField] GameObject WinScreen;
    [SerializeField] GameObject LoseScreen;
    [SerializeField] TextMeshPro MoneyText;
    [SerializeField] Resource ResourceScript;
    [SerializeField] HP HPScript;
    [SerializeField] SFX SFXScript;
    [SerializeField] AudioClip PlaySFX;
    private bool isWin;

    private void Awake()
    {
        Time.timeScale = 1;
        isWin = false;

        if (PlayerDataManager.instance.hasSaveData)
        {
            HPScript.SetHP(PlayerDataManager.instance.saveHP);
            ResourceScript.SetMoney(PlayerDataManager.instance.saveMoney);
        }
        else if (PlayerDataManager.instance.hasSaveData == false)
        {
            HPScript.SetHP(15);
            ResourceScript.SetMoney(2000);
        }

    }

    private void Start()
    {
        Phase = GameState.Prep;
    }

    private void Update()
    {
        PrepPhase();
        Movable();
        GoalCheck();

        MoneyText.text = ResourceScript.money.ToString();
        if (Input.GetKeyDown(KeyCode.G))
        {
            ResourceScript.Income(500);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PlayerDataManager.instance.ClearPlayerData();
            SceneManager.LoadScene("MainMenu");
        }
    }

    private void PrepPhase()
    {
        //Planning.
        if (Input.GetKeyDown(KeyCode.Space) && Phase == GameState.Prep)
        {
            SFXScript.PlaySound(PlaySFX);
            Phase = GameState.Play;
        }
    }

    private void Movable()
    {
        if (Phase != GameState.Play)
        {
            return;
        }

        PlayerScript.Movement();

        //Player can move.

    }

    private void GoalCheck()
    {
        GameObject Player = GameObject.Find("Player");
        Transform PlayerTransform = Player.GetComponent<Transform>();

        if (PlayerTransform.position == Goal_1.position)
        {
            isWin = true;
        }

        if (PlayerTransform.position == Goal_2.position)
        {
            isWin = true;
        }

        if (HPScript.PlayerHP <= 0)
        {
            LoseScreen.SetActive(true);
            Time.timeScale = 0;
            Restart();
        }

        if (isWin == true)
        {
            WinScreen.SetActive(true);
            Time.timeScale = 0;
            NextLevel();
            PlayerDataManager.instance.SavePlayerData(HPScript.PlayerHP, ResourceScript.money);

            //if (PlayerDataManager.instance.hasSaveData)
            //{
            //    HPScript.SetHP(PlayerDataManager.instance.saveHP);
            //    ResourceScript.SetMoney(PlayerDataManager.instance.saveMoney);
            //}
        }
    }

    private void Restart()
    {
        if (Input.GetKeyUp(KeyCode.Backspace))
        {
            PlayerDataManager.instance.ClearPlayerData();
            SceneManager.LoadScene("Level_01");
        }
    }

    private void NextLevel()
    {
        if (Input.GetKeyUp(KeyCode.Return))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
