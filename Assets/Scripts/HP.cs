using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public class HP : MonoBehaviour
{
    [SerializeField] int _playerHP;
    public int PlayerHP => _playerHP;
    [SerializeField] TextMeshPro HPText;

    private void Update()
    {
        HPText.text = _playerHP.ToString();
    }

    public void loseHP(int Damage)
    {
        _playerHP -= Damage;
    }

    public void SetHP(int HP)
    {
        _playerHP = HP;
    }
}
