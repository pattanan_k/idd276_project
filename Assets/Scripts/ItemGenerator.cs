using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{

    [SerializeField]
    Transform[] SpawnPoints;

    [SerializeField]
    GameObject[] Items;

    [SerializeField]
    LayerMask itemLayer;

    [SerializeField]
    SFX SFX;

    [SerializeField]
    AudioClip PaySFX;

    private GameObject GameSystem;
    public GameObject SystemScript => GameSystem;

    void Start()
    {
        GameSystem = GameObject.Find("System");
    }

    void FixedUpdate()
    {
        //currentItems = GameObject.FindGameObjectsWithTag("Item");
        PutItemsinShop();
    }

    void Update()
    {
        Reroll();
    }

    void PutItemsinShop()
    {
        void PlaceItems(int SP)
        {
            GameObject SpawnItem = Instantiate(Items[Random.Range(0, 6)]);
            SpawnItem.transform.position = SpawnPoints[SP].position;
        }

        if (!Physics2D.OverlapCircle(SpawnPoints[0].position, .2f, itemLayer))
        {
            PlaceItems(0);
            return;
        }

        if (!Physics2D.OverlapCircle(SpawnPoints[1].position, .2f, itemLayer))
        {
            PlaceItems(1);
            return;
        }

        if (!Physics2D.OverlapCircle(SpawnPoints[2].position, .2f, itemLayer))
        {
            PlaceItems(2);
            return;
        }

        if (!Physics2D.OverlapCircle(SpawnPoints[3].position, .2f, itemLayer))
        {
            PlaceItems(3);
            return;
        }

        if (!Physics2D.OverlapCircle(SpawnPoints[4].position, .2f, itemLayer))
        {
            PlaceItems(4);
            return;
        }
    }

    void Reroll()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(1))
        {
            if (Physics2D.OverlapPoint(mousePosition, itemLayer))
            {
                SFX.PlaySound(PaySFX);
                SystemScript.SendMessage("Payment", 10);
                Destroy(Physics2D.OverlapPoint(mousePosition, itemLayer).gameObject);
            }
        }
    }
}
