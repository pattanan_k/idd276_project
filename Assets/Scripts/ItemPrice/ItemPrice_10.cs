using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPrice_10 : DragDrop
{
    public override void OnMouseUp()
    {
        isDragging = false;
        DropSFX.Play();

        if (Paid == false)
        {
            SystemScript.SendMessage("Payment", 15);
            PaySFX.Play();
            Paid = true;
        }
    }
}
