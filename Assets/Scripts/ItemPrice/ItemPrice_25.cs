using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPrice_25 : DragDrop
{
    public override void OnMouseUp()
    {
        isDragging = false;
        DropSFX.Play();

        if (Paid == false)
        {
            SystemScript.SendMessage("Payment", 45);
            PaySFX.Play();
            Paid = true;
        }
    }
}
