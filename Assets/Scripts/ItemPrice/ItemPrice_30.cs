using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPrice_30 : DragDrop
{
    public override void OnMouseUp()
    {
        isDragging = false;
        DropSFX.Play();

        if (Paid == false)
        {
            SystemScript.SendMessage("Payment", 60);
            PaySFX.Play();
            Paid = true;
        }
    }
}
