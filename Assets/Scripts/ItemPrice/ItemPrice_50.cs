using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPrice_50 : DragDrop
{
    public override void OnMouseUp()
    {
        isDragging = false;
        DropSFX.Play();

        if (Paid == false)
        {
            SystemScript.SendMessage("Payment", 100);
            PaySFX.Play();
            Paid = true;
        }
    }
}
