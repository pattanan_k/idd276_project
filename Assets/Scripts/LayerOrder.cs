using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerOrder : MonoBehaviour
{
    [SerializeField] int pushLayer;

    void Start()
    {
        GetComponent<Renderer>().sortingOrder = pushLayer;
    }
}
