using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MessageCoroutine : MonoBehaviour
{
    [SerializeField] GameObject _messageFolder;
    [SerializeField] TextMeshPro _messageText;

    float messageDuration = 1.2f;

    IEnumerator MessageTurnOn()
    {
        _messageFolder.SetActive(true);
        yield return new WaitForSeconds(messageDuration);
        _messageFolder.SetActive(false);
    }

    public void DisplayGainMessage(string _message)
    {
        _messageText.text = $"<color=#00FF4F>{_message}</color>" + "(G) Gain";
        StartCoroutine(MessageTurnOn());
    }

    public void DisplayLostMessage(string _message)
    {
        _messageText.text = $"<color=#D44B4B>{_message}</color>" + "(G) Lost";
        StartCoroutine(MessageTurnOn());
    }
}
