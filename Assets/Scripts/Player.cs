using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float freezeMovement;
    [SerializeField] Transform movePoint;
    [SerializeField] LayerMask collide;
    [SerializeField] SFX SFXscript;

    public AudioClip MoveSFX;


    void Start()
    {
        movePoint.parent = null;
    }

    void Update()
    {
        
    }

    public void Movement()
    {
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);

        freezeMovement += Time.deltaTime;

        if (Vector3.Distance(transform.position, movePoint.position) <= 0.5f)
        {
            if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f)
            {
                if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(Input.GetAxisRaw("Horizontal"), 0f, 0f), .2f, collide)
                    && freezeMovement > 0.35f)
                {
                    movePoint.position += new Vector3(Input.GetAxisRaw("Horizontal"), 0f, 0f);
                    freezeMovement = 0f;
                    SFXscript.PlaySound(MoveSFX);
                }
            }
            else if (Mathf.Abs(Input.GetAxisRaw("Vertical")) == 1f)
            {
                if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(0f, Input.GetAxisRaw("Vertical"), 0f), .2f, collide)
                    && freezeMovement > 0.35f)
                {
                    movePoint.position += new Vector3(0f, Input.GetAxisRaw("Vertical"), 0f);
                    freezeMovement = 0f;
                    SFXscript.PlaySound(MoveSFX);
                }
            }
        }
    }
}
