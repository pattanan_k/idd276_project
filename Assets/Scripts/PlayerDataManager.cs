using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : MonoBehaviour
{
    public static PlayerDataManager instance;

    public int saveHP { get; private set; }
    public int saveMoney { get; private set; }
    public bool hasSaveData { get; private set; }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    public void SavePlayerData(int HP, int Money)
    {
        saveHP = HP;
        saveMoney = Money;
        hasSaveData = true;
    }

    public void ClearPlayerData()
    {
        hasSaveData = false;
    }

}
