using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGuide : MonoBehaviour
{
    [SerializeField] private GameObject Guide;
    [SerializeField] private float Xoffset;
    [SerializeField] private float Yoffset;
    private Vector2 mousePosition;

    private void Start()
    {
        Guide.SetActive(false);
    }

    private void Update()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private void OnMouseOver()
    {
        Guide.transform.position = mousePosition + new Vector2(Xoffset, Yoffset);
        Guide.SetActive(true);
    }

    private void OnMouseExit()
    {
        Guide.SetActive(false);
    }
}
