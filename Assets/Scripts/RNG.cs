using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RNG : MonoBehaviour
{
    [SerializeField] int successRate;
    [SerializeField] Transform ItemPosition;
    [SerializeField] LayerMask trap;
    [SerializeField] GameObject PositionChecker;
    private float Radius = .2f;

    void Start()
    {
        
    }

    void Update()
    {
        PositionCheck();
        
        GameObject PhaseScript = GameObject.Find("System");
        GamePhase GamePhaseScript = PhaseScript.GetComponent<GamePhase>();

        if (Physics2D.OverlapCircle(ItemPosition.position, Radius, trap) && GamePhaseScript.Phase == GameState.Play)
        {
            ChanceCalculator();
        }
    }

    private void ChanceCalculator()
    {
        int randomNumber = Random.Range(1, 101);

        if (successRate >= randomNumber)
        {
            Debug.Log(randomNumber + " Win.");
            Destroy(Physics2D.OverlapCircle(ItemPosition.position, Radius, trap).gameObject);
            Destroy(this.gameObject);
        }
        else if (successRate < randomNumber)
        {
            Debug.Log(randomNumber + " Lose.");
            Destroy(this.gameObject);
        }
    }

    private void PositionCheck()
    {
        if (Physics2D.OverlapCircle(ItemPosition.position, Radius, trap))
        {
            PositionChecker.SetActive(true);
        }

        if (!Physics2D.OverlapCircle(ItemPosition.position, Radius, trap))
        {
            PositionChecker.SetActive(false);
        }
    }
}
