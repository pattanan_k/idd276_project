using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEvent : MonoBehaviour
{
    private GameObject _money;
    public GameObject Money => _money;

    [SerializeField] LayerMask player;

    [SerializeField] MessageCoroutine _messageCoroutine;

    void Start()
    {
        _money = GameObject.Find("System");
    }
    
    void Update()
    {
        if (Physics2D.OverlapCircle(this.gameObject.transform.position, .2f, player))
        {
            RandomMoneyEffect();
            Destroy(this.gameObject);
        }
    }

    public void RandomMoneyEffect()
    {
        int _randomizedNum = Random.Range(1, 201);

        //get effect based on chance
        if (_randomizedNum > 0 && _randomizedNum <= 45)
        {
            Money.SendMessage("Income", 50);
            _messageCoroutine.DisplayGainMessage("50");
        }
        else if (_randomizedNum > 45 && _randomizedNum <= 80)
        {
            Money.SendMessage("Income", 100);
            _messageCoroutine.DisplayGainMessage("100");
        }
        else if (_randomizedNum > 80 && _randomizedNum <= 100)
        {
            Money.SendMessage("Income", 200);
            _messageCoroutine.DisplayGainMessage("200");
        }
        else if (_randomizedNum > 100 && _randomizedNum <= 145)
        {
            Money.SendMessage("Payment", 50);
            _messageCoroutine.DisplayLostMessage("50");
        }
        else if (_randomizedNum > 145 && _randomizedNum <= 180)
        {
            Money.SendMessage("Payment", 100);
            _messageCoroutine.DisplayLostMessage("100");
        }
        else if (_randomizedNum > 180 && _randomizedNum <= 200)
        {
            Money.SendMessage("Payment", 200);
            _messageCoroutine.DisplayLostMessage("200");
        }
    }
}
