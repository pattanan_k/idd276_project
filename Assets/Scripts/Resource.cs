using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Resource : MonoBehaviour
{
    [SerializeField] int Money;
    public int money => Money;

    public void Payment(int amount)
    {
        Money -= amount;
    }

    public void Income(int _amount)
    {
        Money += _amount;
    }

    public void SetMoney(int Resource)
    {
        Money = Resource;
    }
}
