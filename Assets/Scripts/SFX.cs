using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public AudioSource SFXsource;
    void Start()
    {
        
    }

   public void PlaySound(AudioClip audio)
    {
        SFXsource.PlayOneShot(audio);
    }
}
