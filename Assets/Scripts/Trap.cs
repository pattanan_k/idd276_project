using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Trap : MonoBehaviour
{
    private GameObject GameSystem;
    public GameObject SystemScript => GameSystem;

    [SerializeField] LayerMask player;
    [SerializeField] SFX SFXscript;

    [SerializeField] AudioClip DamageSFX;

    void Start()
    {
        GameSystem = GameObject.Find("System");
    }

    void Update()
    {
        if (Physics2D.OverlapCircle (this.gameObject.transform.position, .2f, player))
        {
            SFXscript.PlaySound(DamageSFX);
            dealDamage();
        }
    }

    public abstract void dealDamage();
}
