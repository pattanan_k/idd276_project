using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage_1 : Trap
{
    public override void dealDamage()
    {
        SystemScript.SendMessage("loseHP", 1);
        Destroy(this.gameObject);
    }
}
