using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage_2 : Trap
{
    public override void dealDamage()
    {
        SystemScript.SendMessage("loseHP", 2);
        Destroy(this.gameObject);
    }
}
