using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage_3 : Trap
{
    public override void dealDamage()
    {
        SystemScript.SendMessage("loseHP", 3);
        Destroy(this.gameObject);
    }
}
