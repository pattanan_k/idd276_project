using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage_4 : Trap
{
    public override void dealDamage()
    {
        SystemScript.SendMessage("loseHP", 5);
        Destroy(this.gameObject);
    }
}
