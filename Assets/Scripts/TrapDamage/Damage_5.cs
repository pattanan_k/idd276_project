using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage_5 : Trap
{
    public override void dealDamage()
    {
        SystemScript.SendMessage("loseHP", 10);
        Destroy(this.gameObject);
    }
}
